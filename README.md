# Almendra Café

This is where we store config about Almendra Café

## Deploy

### Deploy landing page

```
kubectl apply -f deploy/almendra.cafe.yaml
```

### Deploy blog

```
kubectl apply -f deploy/blog.almendra.cafe.yaml
```
